# design-system-docs

## Project setup
```
git submodule init && git submodule update
yarn install
```

## Rodar a documentação localmente
```
yarn run styleguide
```

## Como documentar um componente

Utilize um arquivo .md com o mesmo nome do arquivo do componente. Além disso, é possível (e recomendado) também documentar as props e métodos dos componentes utilizando comentários. [Mais informações](https://vue-styleguidist.github.io/Documenting.html#using-jsdoc-tags).

## Fez alterações no DLS?
Importante dar push no submodule primeiro!
```
cd src/dls
git add .
git commit -m "Explicar alterações realizadas"
git push origin master
cd ../../
git add .
git commit -m "DLS atualizado"
git push origin master
```


