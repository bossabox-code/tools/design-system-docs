

Esta é a documentação do design system interno da BossaBox, feito em VueJS. Para sua utilização, deve-se importar o repositório `design-system` contendo o código fonte de todos os componentes e folhas de estilo utilizados.

### Requisitos

O Design System contém componentes e folhas de estilo com classes pré-definidas. Tais componentes utilizam as classes globais contidas nas folhas de estilo, e portanto alguns componentes não se mostrarão corretos se as classes utilizadas não estiverem importadas. Cada componente contém em sua descrição as folhas de estilo e pacotes externos necessários para seu funcionamento.
