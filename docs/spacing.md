

O arquivo `css/spacing.scss` contém as classes de espaçamento.

As classes de espaçamento são:

- Margem: 
    - `.margin[-$breakpoint]-$size`, 
    - `.margin-vertical[-$breakpoint]-$size`, 
    - `.margin-horizontal[-$breakpoint]-$size`, 
    - `.margin-top[-$breakpoint]-$size`
    - `.margin-right[-$breakpoint]-$size`
    - `.margin-bottom[-$breakpoint]-$size`
    - `.margin-left[-$breakpoint]-$size`

- Padding:
    - `.padding[-$breakpoint]-$size`, 
    - `.padding-vertical[-$breakpoint]-$size`, 
    - `.padding-horizontal[-$breakpoint]-$size`, 
    - `.padding-top[-$breakpoint]-$size`
    - `.padding-right[-$breakpoint]-$size`
    - `.padding-bottom[-$breakpoint]-$size`
    - `.padding-left[-$breakpoint]-$size`


(elementos entre `[colchetes]` não são obrigatórios)

onde `$size` pode ser um dos seguintes valores (e seus significados):

- `0`: 0,
- `auto`: auto,
- `xxtiny`: 2px,
- `xtiny`: 4px,
- `tiny`: 8px,
- `normal`: 16px,
- `big`: 32px,
- `xbig`: 64px,
- `xxbig`: 128px,
- `huge`: 256px
