
O arquivo `css/grid.scss` contém as classes globais de grid.

O grid contém as classes básicas de `.content` e `.row`. 
- `.content` define um width de `896px` e margens laterais `auto`.
- `.row` deve ser utilizado sempre que o elemento irá conter colunas.

O grid é dividido em 12 colunas. A classe para colunas é `col-$i`, onde `$i` é o tamanho da coluna baseado em um grid de 12 partes.

### Gutter

É possível definir um _gutter_ para as colunas (espaçamento horizontal entre elas). Isto é feito adicionando uma classe de `.cols-gutter-$size` à `.row` contendo as colunas, onde `$size` é algum dos seguintes tamanhos:
- `0`: 0
- `xxtiny`: 2px
- `xtiny`: 4px
- `tiny`: 8px
- `normal`: 16px
- `big`: 32px
- `xbig`: 64px
- `xxbig`: 128px
- `huge`: 256px

### Gap

É possível definir um _gap_ para as colunas (espaçamento vertical entre elas). Isto é feito adicionando uma classe de `.cols-gap-$size` à `.row` contendo as colunas, onde `$size` é um dos tamanhos citados acima.

### Offset

É possível definir um _offset_ para uma coluna (empurrar para direita em `i` partes). Exemplo: `.col-offset-$i`.

### Breakpoints

Todas as classes de `col`, `gutter`, `gap`, `offset` e outras mostradas mais abaixo podem ser modificadas para serem aplicadas somente em _breakpoints_ (tamanhos de tela específicos). Os _breakpoints_ são:

- `sm`: 576px ou maior
- `md`: 768px ou maior
- `lg`: 992px ou maior
- `xl`: 1200px ou maior

Exemplo de modificações das classes utilizando _breakpoints_:

- cols: `.col-$breakpoint-$i`
- gutter: `.cols-$breakpoint-gutter-$size`
- gap: `.cols-$breakpoint-gap-$size`
- offset: `.col-offset-$breakpoint-$i`

