
Para utilizar os componentes e classes do design-system em outros projetos, basta importar os arquivos do repositório [design-system](https://gitlab.com/bossabox-code/tools/design-system) em alguma pasta do projeto, como em `src/dls`, e importar os componentes e seus requisitos quando necessário.

### Submodule/subtree

É possível importar o conteúdo do repositório do código fonte em outro projeto utilizando `submodule` ou `subtree`.

- submodule: `git submodule add git@gitlab.com:bossabox-code/tools/design-system.git src/dls`
- subtree: `git subtree pull --prefix src/dls git@gitlab.com:bossabox-code/tools/design-system.git master`

