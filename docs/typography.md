
As classes de tipografia estão contidas no arquivo `css/typography.scss`.

O arquivo aplica uma `font-size` de `15px` ao _html_ e _body_.

As classes de tipografia são:
- `.font-size[-$breakpoint]-$size`: aplica `font-size: $size`, onde `$size` pode ser uma das (e seus respectivos valores):
    - `tiny`: 0.875rem
    - `normal`: 1rem
    - `big`: 1.125rem
    - `xbig`: 1.25rem
    - `xxbig`: 1.5rem
    - `huge`: 1.75rem
    - `xhuge`: 2rem
- `.strong`, ou `.bold`: aplica `font-weight: bold`
- `.align-right[-$breakpoint]`: aplica `text-align: right`;
- `.align-left[-$breakpoint]`: aplica `text-align: left`;
- `.align-center[-$breakpoint]`: aplica `text-align: center`;


(elementos entre `[colchetes]` não são obrigatórios)
