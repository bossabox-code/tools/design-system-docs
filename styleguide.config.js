var path = require('path');
module.exports = {
	// set your styleguidist configuration here
	title: 'BossaBox Design System',
	components: ['src/**/[A-Z]*.svg', 'src/**/[A-Z]*.vue' ],
	require: [
		// Components style
		path.join(__dirname, 'src/dls/css/styling.scss'),
		path.join(__dirname, 'src/dls/css/elevation.scss'),
		path.join(__dirname, 'src/dls/css/grid.scss'),
		path.join(__dirname, 'src/dls/css/position.scss'),
		path.join(__dirname, 'src/dls/css/spacing.scss'),
		path.join(__dirname, 'src/dls/css/typography.scss'),
		// Custom css for docs
		path.join(__dirname, 'src/css/docs.custom.scss'),
	],
	sections: [
		{
			name: 'Introdução',
			content: 'docs/introduction.md'
		},
		{
			name: 'Instalação',
			content: 'docs/installation.md',
		},
		{
			name: 'Classes',
			sections: [
				{
					name: 'Grid & Breakpoints',
					description: 'Classes de grid e tamanhos de tela',
					content: 'docs/grid.md'
				},
				{
					name: 'Positioning',
					description: 'Classes de posicionamento (`display`, `flex`)',
					content: 'docs/positioning.md'
				},
				{
					name: 'Spacing',
					description: 'Classes de espaçamento (`margin`, `padding`)',
					content: 'docs/spacing.md'
				},
				{
					name: 'Colors',
					description: 'Classes de cores (`background`, `border`, `color`)',
					content: 'docs/colors.md'
				},
				{
					name: 'Typography',
					description: 'Classes de tipografia (`font-size`, `font-weight`)',
					content: 'docs/typography.md'
				},
				{
					name: 'Elevation',
					description: 'Classes de sombreamento e elevanção (`box-shadow`, `transform`)',
					content: 'docs/elevation.md'
				},

			]
		},
		{
			name: 'Componentes',
			components: ['src/**/[A-Z]*.svg', 'src/**/[A-Z]*.vue' ],
		}
	]
	// sections: [
	//   {
	//     name: 'First Section',
	//     components: 'src/components/**/[A-Z]*.vue'
	//   }
	// ],
	// webpackConfig: {
	//   // custom config goes here
	// }
}
